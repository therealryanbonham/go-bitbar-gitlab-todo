package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLeftPad(t *testing.T) {
	s := leftPadTrimString("test", 10)
	assert.Equal(t, s, "      test", "Left Padding should have make this 10 chars with spaces on left.")
}

func TestLeftPadExceedLength(t *testing.T) {
	s := leftPadTrimString("testing length", 10)
	assert.Equal(t, s, "testing...", "Left Padding max chars of 10 with elipse.")
}

func TestRightPad(t *testing.T) {
	s := rightPadTrimString("test", 10)
	assert.Equal(t, s, "test      ", "Right Padding should have make this 10 chars with spaces on left.")
}

func TestShouldIgnoreProjectFalse(t *testing.T) {
	found := shouldIgnoreProject([]string{"^test/first"}, "test/second")
	assert.False(t, found, "Expected found to be False")
}

func TestShouldIgnoreProjectFalseFullString(t *testing.T) {
	found := shouldIgnoreProject([]string{"^test/second$"}, "test/secondary")
	assert.False(t, found, "Expected found to be False")
}

func TestShouldIgnoreProjectTrue(t *testing.T) {
	found := shouldIgnoreProject([]string{"^test/second$"}, "test/second")
	assert.True(t, found, "Expected found to be True")
}

func TestShouldIgnoreProjectPartialTrue(t *testing.T) {
	found := shouldIgnoreProject([]string{"test/.*"}, "test/second")
	assert.True(t, found, "Expected found to be True")
}

func TestRightPadExceedLength(t *testing.T) {
	s := rightPadTrimString("testing length", 10)
	assert.Equal(t, s, "testing...", "Right Padding max chars of 10 with elipse.")
}

func TestLabelString(t *testing.T) {
	allowedLabels := []string{"DNM", "Critical"}
	targetLabels := []string{"Security", "DNM", "Critical", "Debt"}
	expectedString := "Critical,DNM"
	s := getLabelString(allowedLabels, targetLabels)
	assert.Equal(t, s, expectedString, "Label Strings should match")
}

func TestContainsStringsFalse(t *testing.T) {
	found := containsStrings("I am purple", []string{"blue", "green"})
	assert.False(t, found, "Expected found to be False")
}

func TestContainsStringsTrue(t *testing.T) {
	found := containsStrings("I am purple", []string{"blue", "green", "purple"})
	assert.True(t, found, "Expected found to be True")
}

func TestFilterLabelsTrue(t *testing.T) {
	allowedLabels := []string{"DNM", "Critical"}
	targetLabels := []string{"Security", "DNM", "Critical", "Debt"}
	s := filterLabels(allowedLabels, targetLabels)
	assert.True(t, s, "Label Strings should match")
}

func TestFilterLabelsFalse(t *testing.T) {
	allowedLabels := []string{"Not Me", "Test"}
	targetLabels := []string{"Security", "DNM", "Critical", "Debt"}
	s := filterLabels(allowedLabels, targetLabels)
	assert.False(t, s, "Label Strings should match")
}
