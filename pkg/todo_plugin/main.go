package main

import (
	"fmt"
	"github.com/johnmccabe/go-bitbar"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"slices"
	"sort"
	"strings"

	"github.com/adam-hanna/arrayOperations"
	"github.com/creasty/defaults"
	"github.com/logrusorgru/aurora"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
)

type configuration struct {
	CloseTodosWithMergedClosedMRs     bool     `default:"false"`
	CloseTodosWithoutRequiredApproval bool     `default:"false"`
	FilterIssueLabels                 []string `default:"[]"`
	FilterMRLabels                    []string `default:"[]"`
	FilterOutKeyWordsIssue            []string `default:"[]"`
	FilterOutKeyWordsMR               []string `default:"[]"`
	FontFamily                        string   `default:"Menlo-Regular"`
	FontSize                          int      `default:"12"`
	GitlabIcon                        string   `default:"iVBORw0KGgoAAAANSUhEUgAAACIAAAAgAQMAAABNQTiKAAABG2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS41LjAiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIi8+CiA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgo8P3hwYWNrZXQgZW5kPSJyIj8+Gkqr6gAAAYJpQ0NQc1JHQiBJRUM2MTk2Ni0yLjEAACiRdZHfK4NRGMc/G/Jroka5cLE0rkxDLW6ULaGkNVOGm+3du01t8/a+k5Zb5VZR4savC/4CbpVrpYiUlDvXxA3r9by22pI9p+c8n/M953k65zlgD6eVjFHrhUw2p4cm/K75yIKr/oVGWnDSgTuqGNpYMDhNVfu8x2bFW49Vq/q5f605rhoK2BqERxVNzwlPCk+v5TSLd4TblVQ0Lnwm3KfLBYXvLD1W5FeLk0X+tlgPhwJgbxN2JSs4VsFKSs8Iy8txZ9KrSuk+1kscanZuVmK3eBcGISbw42KKcQL4GGBEZh8eBumXFVXyvb/5M6xIriKzRh6dZZKkyNEn6qpUVyUmRFdlpMlb/f/bVyMxNFis7vBD3bNpvvdA/TYUtkzz68g0C8dQ8wSX2XL+yiEMf4i+VdbcB9C6AedXZS22Cxeb0PmoRfXor1Qjbk8k4O0UWiLgvIGmxWLPSvucPEB4Xb7qGvb2oVfOty79AERNZ9aX3fKfAAAABlBMVEUAAAAmRcn2EULJAAAAAnRSTlP/AOW3MEoAAAAJcEhZcwAAFiUAABYlAUlSJPAAAABmSURBVAiZY/gPBAcYkMm////vRyV//P8vj0p+qP/Hj0o+sP/DDjfnAT+QPP+B/zOYfHyA4TiYbP4hf/AAw8Ef8s1A9T/kgbp+/LEHmvanxs4eqMvGBqR3XiWIvFcMIv/Vo7kNgwQA44R8QJN1/JwAAAAASUVORK5CYII="`
	GitLabToken                       string
	GitLabURL                         string `default:"https://gitlab.com/api/v4"`
	GitLabUserName                    string
	headerColor                       string   `default:"#444444"`
	HideMRFailingPipeline             bool     `default:"true"`
	HideMRRunningPipeline             bool     `default:"true"`
	IgnoreRepos                       []string `default:"[]"`
	PriorityMRGroup                   []int    `default:"[]"`
	PriorityMRKeyWords                []string `default:"[]"`
	PriorityMRMinApprovers            int      `default:"0"`
	ShowIssueLabels                   []string `default:"[]"`
	ShowMRLabels                      []string `default:"[]"`
	ShowMRPipelineStatus              bool     `default:"false"`
	ShowPriorityIconOnMenubar         bool     `default:"false"`
	IgnoreMergeRequestApprovalRules   []string `default:"[]"`
}

var hasPriority int
var menuText string

func getTodoCount(g *gitlab.Client) int {
	todos, _, err := g.Todos.ListTodos(&gitlab.ListTodosOptions{})
	if err != nil {
		log.Fatal(err)
	}
	return len(todos)
}

func leftPadTrimString(s string, p int) string {
	if len(s) > p {
		trim := p - 3
		s = s[:trim] + "..." // slicing is a constant time operation in go
	}
	return fmt.Sprintf("%*s", p, s)
}

func rightPadTrimString(s string, p int) string {
	if len(s) > p {
		trim := p - 3
		s = s[:trim] + "..." // slicing is a constant time operation in go
	}
	return fmt.Sprintf("%-*s", p, s)
}

func filterLabels(allowedLabels []string, targetLabels []string) bool {
	z, ok := arrayOperations.Intersect(allowedLabels, targetLabels)
	if !ok {
		fmt.Println("Cannot find intersect")
	}

	slice, ok := z.Interface().([]string)
	if !ok {
		fmt.Println("Cannot convert to slice")
	}
	if len(slice) > 0 {
		return true
	}
	return false
}

func getLabelString(allowedLabels []string, targetLabels []string) string {
	z, ok := arrayOperations.Intersect(allowedLabels, targetLabels)
	if !ok {
		fmt.Println("Cannot find intersect")
	}

	slice, ok := z.Interface().([]string)
	if !ok {
		fmt.Println("Cannot convert to slice")
	}
	sort.Strings(slice)
	return strings.Join(slice, ",")

}
func isInSlice(slice []int, val int) bool {
	// Simple, yet handsome function to check if an integer occurs in a slice of integers
	for _, item := range slice {
		if item == val {
			return true
		}
	}
	return false
}

func getApprovalState(g *gitlab.Client, c *configuration, t *gitlab.Todo) (*gitlab.MergeRequestApprovalState, error) {
	mrApprovalState, _, err := g.MergeRequestApprovals.GetApprovalState(t.Target.ProjectID, t.Target.IID)
	return mrApprovalState, err
}

func closeTodosWithoutRequiredApproval(g *gitlab.Client, c *configuration, t *gitlab.Todo, mrApprovalState *gitlab.MergeRequestApprovalState) bool {
	var found = false
	for _, r := range mrApprovalState.Rules {
		if !slices.Contains(c.IgnoreMergeRequestApprovalRules, r.Name) {
			required := r.ApprovalsRequired
			if required > 0 {
				for _, u := range r.EligibleApprovers {
					if u.Username == c.GitLabUserName {
						found = true
						break
					}
				}
			}
		}
	}
	if found == false && c.CloseTodosWithoutRequiredApproval {
		_, err := g.Todos.MarkTodoAsDone(t.ID)
		if err != nil {
			log.Fatal(err)
		}
		return true
	}
	return false
}
func getMRPriority(g *gitlab.Client, c *configuration, t *gitlab.Todo, mrApprovalState *gitlab.MergeRequestApprovalState) bool {
	//Check Keywords first as this is faster then the Approver Check
	if containsStrings(t.Target.Title, c.PriorityMRKeyWords) {
		return true
	}

	for _, r := range mrApprovalState.Rules {
		if !slices.Contains(c.IgnoreMergeRequestApprovalRules, r.Name) {
			required := r.ApprovalsRequired
			eligable := len(r.EligibleApprovers)
			var found bool
			for _, u := range r.EligibleApprovers {
				if u.Username == c.GitLabUserName {
					found = true
					break
				}
			}
			if found {
				diff := eligable - required
				if diff <= c.PriorityMRMinApprovers {
					return true
				}
			}
			// Check to see if the int group.ID occurs in the slice c.PriorityMRGroup
			for _, group := range r.Groups {
				if isInSlice(c.PriorityMRGroup, group.ID) {
					return true
				}
			}
		}
	}

	return false

}
func getMRPipelineStatusIcon(status string) string {
	var pipelineIcon = leftPadTrimString("No Pipeline", 15)
	switch status {
	case "running":
		pipelineIcon = aurora.Sprintf(aurora.Yellow(leftPadTrimString("Running", 15)))
	case "failed":
		pipelineIcon = aurora.Sprintf(aurora.Red(leftPadTrimString("Failed", 15)))
	case "success":
		pipelineIcon = aurora.Sprintf(aurora.Green(leftPadTrimString("Success", 15)))
	}
	return pipelineIcon
}
func getMRPipelineStatus(g *gitlab.Client, c *configuration, t *gitlab.Todo) string {
	var status = ""
	pipelines, _, err := g.MergeRequests.ListMergeRequestPipelines(t.Target.ProjectID, t.Target.IID)
	if err != nil {
		log.Fatal(err)
	}
	if len(pipelines) > 0 {
		p := *pipelines[0]
		status = p.Status
	}
	return status
}

func containsStrings(s string, subs []string) bool {
	for _, sub := range subs {
		if strings.Contains(s, sub) {
			return true
		}
	}
	return false
}

func printIssues(g *gitlab.Client, c *configuration, b *bitbar.SubMenu) int {
	count := 0
	//Print Header
	msg := fmt.Sprintf("%s %s %s", rightPadTrimString("Project", 20), leftPadTrimString("Issue Title", 35), leftPadTrimString("Status", 10))
	if len(c.ShowIssueLabels) > 0 {
		labels := leftPadTrimString("Labels", 15)
		msg = fmt.Sprintf("%s %s", msg, labels)
	}
	b.Line(msg).Font(c.FontFamily).Size(c.FontSize).Color(c.headerColor)
	b.HR()

	opt := &gitlab.ListTodosOptions{ListOptions: gitlab.ListOptions{
		PerPage: 100,
		Page:    1,
	}, Type: gitlab.Ptr("Issue")}

	for {
		todos, resp, err := g.Todos.ListTodos(opt)
		if err != nil {
			log.Fatal(err)
		}

		for _, t := range todos {
			// Right Justify Project Path
			path := rightPadTrimString(t.Project.Path, 20)
			state := leftPadTrimString(t.Target.State, 10)
			desc := leftPadTrimString(t.Target.Title, 35)
			if containsStrings(desc, c.FilterOutKeyWordsIssue) {
				continue
			}
			if shouldIgnoreProject(c.IgnoreRepos, t.Project.PathWithNamespace) {
				continue
			}

			if len(c.FilterIssueLabels) > 0 {
				if filterLabels(c.FilterIssueLabels, t.Target.Labels) {
					continue
				}
			}

			msg := fmt.Sprintf("%s %s %s", path, desc, state)
			if len(c.ShowIssueLabels) > 0 {
				labels := leftPadTrimString(getLabelString(c.ShowIssueLabels, t.Target.Labels), 15)
				msg = fmt.Sprintf("%s %s", msg, labels)
			}

			b.Line(msg).Href(t.TargetURL).Font(c.FontFamily).Size(c.FontSize)
			count++
		}
		// Exit the loop when we've seen all pages.
		if resp.NextPage == 0 {
			break
		}

		// Update the page number to get the next page.
		opt.ListOptions.Page = resp.NextPage
	}
	return count
}

func shouldIgnoreProject(ignorePaths []string, p string) bool {
	for _, i := range ignorePaths {
		sampleRegex := regexp.MustCompile(i)
		match := sampleRegex.Match([]byte(p))
		if match {
			return true
		}
	}
	return false
}

func printMergeRequest(g *gitlab.Client, c *configuration, b *bitbar.SubMenu) int {
	count := 0
	opt := &gitlab.ListTodosOptions{ListOptions: gitlab.ListOptions{
		PerPage: 100,
		Page:    1,
	}, Type: gitlab.Ptr("MergeRequest")}

	//Print Header
	msg := fmt.Sprintf("   %s %s %s", rightPadTrimString("Project", 20), leftPadTrimString("MR Title", 35), leftPadTrimString("Status", 10))
	if c.ShowMRPipelineStatus == true {
		msg = fmt.Sprintf("%s %s", msg, leftPadTrimString("Pipeline", 15))
	}
	if len(c.ShowMRLabels) > 0 {
		labels := leftPadTrimString("Labels", 15)
		msg = fmt.Sprintf("%s %s", msg, labels)
	}
	b.Line(msg).Font(c.FontFamily).Size(c.FontSize).Color(c.headerColor).Trim(false)
	b.HR()
	hasPriority = 0
	//Loop Through Todos
	for {
		todos, resp, err := g.Todos.ListTodos(opt)
		if err != nil {
			log.Fatal(err)
		}
		for _, t := range todos {
			path := rightPadTrimString(t.Project.Path, 20)
			state := leftPadTrimString(t.Target.State, 10)
			desc := leftPadTrimString(t.Target.Title, 35)

			if c.CloseTodosWithMergedClosedMRs && (t.Target.State == "merged" || t.Target.State == "closed") {
				_, err := g.Todos.MarkTodoAsDone(t.ID)
				if err != nil {
					log.Fatal(err)
				}
				continue
			}
			priorityIcon := " "
			mrApprovalState, err := getApprovalState(g, c, t)
			if err == nil {
				if getMRPriority(g, c, t, mrApprovalState) {
					priorityIcon = "❗️"
					// Keep a counter of priority MRs and use this later to display the priority icon in the menubar
					hasPriority++
				}
				if closeTodosWithoutRequiredApproval(g, c, t, mrApprovalState) {
					continue
				}

			} else {
				log.Print(err)
			}
			pipelineStatus := getMRPipelineStatus(g, c, t)
			if c.HideMRFailingPipeline && pipelineStatus == "failed" {
				continue
			}

			if c.HideMRRunningPipeline && pipelineStatus == "running" {
				continue
			}

			pipelineIcon := getMRPipelineStatusIcon(pipelineStatus)

			if len(c.FilterMRLabels) > 0 {
				if filterLabels(c.FilterMRLabels, t.Target.Labels) {
					continue
				}
			}

			if containsStrings(desc, c.FilterOutKeyWordsMR) {
				continue
			}
			if shouldIgnoreProject(c.IgnoreRepos, t.Project.PathWithNamespace) {
				continue
			}

			msg := fmt.Sprintf("%s %s %s %s", priorityIcon, path, desc, state)
			if c.ShowMRPipelineStatus == true {
				msg = fmt.Sprintf("%s %s", msg, pipelineIcon)
			}
			if len(c.ShowMRLabels) > 0 {
				labels := leftPadTrimString(getLabelString(c.ShowMRLabels, t.Target.Labels), 15)
				msg = fmt.Sprintf("%s %s", msg, labels)
			}
			// Setting an alternate msg and line, for marking Todos as done
			// Need to build the URL. This is pretty hacky; I apologize in advance
			url := fmt.Sprintf("%s/todos/%d/mark_as_done", c.GitLabURL, t.ID)
			altMsg := fmt.Sprintf("❌ %s", msg)
			altHeader := fmt.Sprintf("'PRIVATE-TOKEN: %s'", c.GitLabToken)
			altBashParams := []string{"-s", "--request", "POST", "--header", altHeader, url}
			// Create the line and its alternate line
			b.Line(msg).Href(t.TargetURL).Font(c.FontFamily).Size(c.FontSize).Trim(false)
			b.Line(altMsg).Alternate(true).Bash("/usr/bin/curl").Params(altBashParams).Font(c.FontFamily).Size(c.FontSize).Trim(false).Refresh()
			count++
		}
		// Exit the loop when we've seen all pages.
		if resp.NextPage == 0 {
			break
		}

		// Update the page number to get the next page.
		opt.ListOptions.Page = resp.NextPage
	}
	return count
}
func readConfig() *configuration {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)
	viper.SetConfigName("config")
	// Relative path for when using go run
	viper.AddConfigPath("./config")
	// Path to executible for when bitbar is executing binary.
	viper.AddConfigPath(exPath + "/config/")
	conf := &configuration{}
	if err = defaults.Set(conf); err != nil {
		panic(err)
	}
	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}
	err = viper.Unmarshal(&conf)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}
	return conf
}
func main() {
	conf := readConfig()
	git, err := gitlab.NewClient(conf.GitLabToken, gitlab.WithBaseURL(conf.GitLabURL))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	todoCount := getTodoCount(git)
	getTodoCount := 0
	app := bitbar.New()
	menu := app.NewSubMenu()
	countMR := printMergeRequest(git, conf, menu)
	menu.HR()
	countIssue := printIssues(git, conf, menu)
	getTodoCount = countMR + countIssue
	menu.HR()
	menu.Line("Refresh").Refresh()
	//fmt.Println("Refresh | refresh=true")
	menu.Line("Your todos on GitLab").Href("https://gitlab.com/dashboard/todos")
	menuText = fmt.Sprintf(" T %v (%v)", getTodoCount, todoCount)
	if hasPriority > 0 && conf.ShowPriorityIconOnMenubar {
		// if hasPriority is > 0 and ShowPriorityIconOnMenubar is true, then we have at least one priority MR, and we
		// should let everybody know about it
		menuText = fmt.Sprintf(" T❗️ %v (%v)", getTodoCount, todoCount)
	}
	app.StatusLine(menuText).TemplateImage(conf.GitlabIcon)
	//fmt.Println("Your todos on GitLab | href=https://gitlab.com/dashboard/todos")
	app.Render()
}
